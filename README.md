﻿STM32F4mini 2nd node.

The repository consists of the code for implementing CubeSat interconnection protocol. This code is running on one of the three nodes that make up the network. 
The nodes are based on STM32F4 ARM architecture microcontrollers.

Each nodes' code is made up of four major parts/files:
1)Physical layer code - implements functions of RFM69HCW radio module. The code is based on RFM69HCW Arduino library (https://github.com/LowPowerLab/RFM69)
2)Datalink layer code - implements datalayer services such as collision control (CSMA/CA protocol) and flow and error control (Stop-And-Wait-ARQ protocol)
3)Network layer code - implements network layer services such as routing (IPv6 addressing, routing and routing table management (route discovery and etc. using AODV protocol))
4)Application layer code - based on FreeRTOS OS which manages automatic radio communication using physical, datalink and network layers services.