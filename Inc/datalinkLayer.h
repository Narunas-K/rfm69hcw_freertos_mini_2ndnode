#ifndef __DATALINKLAYER_H
#define __DATALINKLAYER_H


#include "main.h"



// CSMA/CA functions prototypes 
bool isChannelIdle(void);

void IFSDelay(void);

uint32_t randomNumGen(uint8_t num_of_tries);

void contentionWindow(uint8_t NumOfSlots);

uint8_t RFM69_sendFrameWithCSMA(uint8_t toAddress, uint8_t* buffer, uint8_t bufferSize);

//StopAndWait functions prototypes
bool sendWithStopAndWait(uint8_t *dataToSend, uint8_t dataToSendSize, uint8_t toAddress);

uint8_t *makeFrames(uint16_t lenghtOfData, uint8_t *dataToSend, uint8_t frameNumber, uint8_t numberOfFrames, uint8_t sequenceNumber);

uint8_t *receiveWithStopAndWait(uint8_t *receivedDataSize, bool acceptBroadcastPackets, uint8_t  *senderMAC );

#define MACBroadcastAddr 255

#endif

