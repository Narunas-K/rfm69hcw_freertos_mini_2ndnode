#include "datalinkLayer.h"

#include "RFM69_STM32.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "rng.h" // random  number generator header file
#include "main.h"

//CSMA/CA IMPLETENTATION BEGIN
int16_t CSMA_threshold = -110;
uint16_t IFS = 10;
uint16_t slotSize = 3; // Contention window slot size in ms
uint16_t CSMATimeout = 2000; // Timeout value after which it is assumed that ACK was corrupted
//uint16_t maxDistance = 1000; //Max distance between two nodes in kilometers (km). Used in calculations of timeout timer value

//Returns TRUE if channel is idle, returns FALSE if channel is busy
bool isChannelIdle(void){
	if(RFM69_receiveDone()){
			if(RFM69Data.RSSI > CSMA_threshold && RFM69Data.TARGETID != MYNODEID){
				printf("Channel is not IDLE! ");
				printf("RSSI: %i\n",RFM69Data.RSSI);
				return false; //Packet received, RSSI above threshold -> channel is busy
			}
			else{
				return true; // RSSI below threshold -> channel is idle
			}
	}
	else{
		return true; //No packet received -> channel is idle
	}
}

void IFSDelay(void){
	HAL_Delay(IFS);
}

//uint8_t num_of_tries
uint32_t randomNumGen(uint8_t num_of_tries){
	//This function uses STM32Fxxx RNG (Random numebr generator), HAL drivers
	uint32_t randomNum  = 0;
	// Calculate CSMA random number upper limit 2^K (2^num_of_tries)
	uint16_t upperNum = (uint16_t) pow(2, num_of_tries);
	HAL_RNG_GenerateRandomNumber(&hrng, &randomNum);
	//printf("Upper: %zu\n", upperNum);
	//printf("%zu\n", randomNum);
	//Divide by 10 and by 2 until randomNUm is lower than 2^K
	while(randomNum > upperNum){
		randomNum = randomNum / 10;
		//randomNum = randomNum >> 1;
		//printf("Shifted %zu\n", randomNum);
	}
	return randomNum;
}

//According CSMA/CA algorithm, after each contention window slot, channel needs to be listened. If channel is found busy, contention window timer is stoped
// and continued after channel is released
void contentionWindow(uint8_t NumOfSlots){
	uint32_t slotStart = 0; // Contention window slot timer variable
	uint32_t slotEnd = 0;
	uint8_t slotCount = 0;
	int32_t slotDifference = 0;
	//printf("R - number of slots: %i\n", NumOfSlots);
	//Contention window begin
	while(slotCount < NumOfSlots){
		//Slot begin
		//printf("Slot begin\n");
		slotStart = millis();
		slotCount++;
		//printf("Slot count:%i\n", slotCount);
		do{
			slotEnd = millis();
			slotDifference = slotEnd - slotStart;
			//printf("SLOT SIZE: %i\n", slotDifference);
		}	
		while((slotDifference < slotSize) && (slotDifference >= 0));
		//printf("Slot end\n");
		//Slot end. If the channel is not idle, stop timer and wait for idle channel
		if(!isChannelIdle()){
			//printf("Channel is not idle after slot\n");
			while(!isChannelIdle()){
			//printf("Kanalas uzimtas-----------------\n");
			}//;
		}
	} // Contention window end
}

bool succeded = true;
uint8_t RFM69_sendFrameWithCSMA(uint8_t toAddress, uint8_t* buffer, uint8_t bufferSize){
	succeded = true;
	uint8_t K = 0; //K - number of tries to send a frame 
	uint8_t R = 0; //R-number of contention windows slots to wait before sending
	uint32_t timerBegin = 0;
	bool goToContWind = false; // variable which allows to proceed to contention window if its value is TRUE
	bool ACKreceived = false; // True if ACK is received
	uint8_t ACKValue[1] = {0};
	//printf("Begining of CSMA/CA algorithm\n");
	do{
		while(!goToContWind){
			//printf("Sense channel 1st time\n");
			while(!isChannelIdle()){
				//printf("uzimta-------------------------\n");
			}//; // Sense channel until channel is found idle
		//	printf("IFS 1st\n");
			IFSDelay(); // Wait IFS time
			//printf("Sense channel 2nd time\n");
			if(!isChannelIdle()){ // if channel is busy sense again and wait IFS time
				goToContWind = false;
				//printf("channel is busy----------------------------\n");
			}
			else{ // if channel is idle proceed next 
				//printf("channel is idle\n");
				goToContWind = true;
			}
		}
		R = randomNumGen(K); // choose random number from 0 o 2^(K-1)
		contentionWindow(R);// wait R slots.
		//if toAddress is broadcast addres do no request ACK, if not request ACK
		if(toAddress == MACBroadcastAddr){
			RFM69_sendFrame(toAddress, buffer, bufferSize, 0, 0); // send frame without ACK request
			printf("Broadcast frame was sent");
		}
		else{
			RFM69_sendFrame(toAddress, buffer, bufferSize, 1, 0); // send frame with ACK request
			printf("Frame sent with ACK request\n");
		}
		//wait for ACK or resend the frame 
		timerBegin = millis();
		while(!ACKreceived && ((millis() - timerBegin) < CSMATimeout)){
			ACKreceived = RFM69_ACKReceived(toAddress);
		}
		if(toAddress == MACBroadcastAddr){
				ACKreceived = true; // set that ACK was received, to avoid resending of broadcast frame
		}
		K++;
		printf("K: %i\n", K);
		if(K ==15){
			succeded = false;
		}
	}
	while(!ACKreceived && (K<15)); // repeat loop till K<15 or ACK received
	//printf("ACK value %i\n", ACKreceived);
	
	printf("Frame was sent successfully within K  %i\n", K);
	
	if(RFM69Data.TARGETID != MACBroadcastAddr){
		ACKValue[0] = RFM69Data.DATA[0];
	//	printf("ACK value %i\n", ACKValue[0]);
		printf("\n");
	}
	else{
		ACKValue[0] = 1;
	//	printf("ACK was assigned to 1, broadcast packet sent");
	}
	
	return ACKValue[0];
}

//uint16_t timerValue(uint16_t maxDistance){
//	uint16_t timeoutValue =  (2 * maxDistance * 1000)/(299792458);
//	return timeoutValue;
//}
//CSMA/CA IMPLEMENTATION END


//STOP AND WAIT IMPLENENTATION BEGIN

//Global variabales definition
uint8_t frameSize = 61; // according to RFM69HCW documentation and be able to use AES encryption
uint8_t sizeOfFrameToSend = 0;// global variable ofholding frame size which is ready to send

bool sendWithStopAndWait(uint8_t *dataToSend, uint8_t dataToSendSize, uint8_t toAddress){
	
	uint8_t frameNumber = 0;
	uint8_t sequenceNumber = 128;
	uint8_t receivedFrameSequenceNumber = 0; 
	uint8_t numberOfFrames = dataToSendSize;
	uint8_t *frameToSendPointer=0;
	uint8_t numberOfTriesToSend = 0;
	//uint8_t *ptrIPpacket=0;
	//ptrIPpacket = (uint8_t*) calloc(payloadLenght+40, sizeof(uint8_t)); // dinamically allocate array of size of IP packet: IP packet lenght=IPv6_header + payloadLength	
	
	//* uint8_t *ptrFrameToSend = 0;
	//* ptrFrameToSend = (uint8_t*) calloc(frameSize, sizeof(uint8_t));
	uint8_t ptrFrameToSend[61];
	
	
	
	//uint8_t frameToSend[22];
	bool ACKReceived= false;
	//uint8_t ACKValue= 0;
	
	// Find number of frames which will be needed to sent data from upper layer
	numberOfFrames = (dataToSendSize) / (frameSize-2); // frameSize-2, because 2 bytes are frame header(sequence number) and trailer
	//CASE: dataToSendSize<frameSize
	if(numberOfFrames == 0){
		numberOfFrames += 1;
	}
	//CASE: dataToSendSize>frameSize and dataToSendSize != frameSize * k ; k - is int
	else if(((dataToSendSize) - (frameSize-2) * numberOfFrames) != 0){
		numberOfFrames += 1;
	}
	//printf("Number of frames after sum: %i\n", numberOfFrames);
	
	while(frameNumber<numberOfFrames){
		//printf("While pradzia \n");
		if(numberOfTriesToSend==1){
			//printf("break---------1\n");
			break;
		}
		sequenceNumber = sequenceNumber ^ 128;
		frameToSendPointer = makeFrames(dataToSendSize, dataToSend, frameNumber, numberOfFrames, sequenceNumber);
		//printf("atejau i cia\n");
		for(int i=0; i<frameSize; i++){
			ptrFrameToSend[i] = 0;
			ptrFrameToSend[i] = *(frameToSendPointer+i);
			//printf("Sukadruota: %i\n", ptrFrameToSend[i]);
		}
		//free(frameToSendPointer);
		printf("Size of frame to send %i\n", sizeOfFrameToSend);
		
		//send frame and wait for ACK
		while(!ACKReceived){
			if(numberOfTriesToSend==1){
				//printf("break---------2\n");
				break;
			}
			printf("Number of tries %i\n", numberOfTriesToSend);
			receivedFrameSequenceNumber = RFM69_sendFrameWithCSMA(toAddress, ptrFrameToSend, sizeOfFrameToSend);
			//printf("ReceivedFrameSequenceNumber %i\n", receivedFrameSequenceNumber&0x80);
			//printf("MySequenceNumber %i\n",sequenceNumber);
			if((receivedFrameSequenceNumber&0x80) == (sequenceNumber^128) || toAddress == MACBroadcastAddr){
				ACKReceived = true;
				numberOfTriesToSend = 0;
			}
			else{
				ACKReceived = false;
				numberOfTriesToSend++;
			}
			printf("Frame which was sent number: %i \n", frameNumber);
			HAL_Delay(50);// wait for ACK
		}
		//printf("ACK REIKSME %i\n",ACKValue);
		//printf("GAVAU ACK!!!\n");
		frameNumber += 1;
		ACKReceived = false; 
	//	printf("Pabaiga while loop\n");
	}
//	printf("free frame to send pointer\n");
	//free(ptrFrameToSend);
//	printf("after free frame to send pointer\n");
	if(numberOfTriesToSend > 0 || succeded ==false){
		printf("Return false\n");
		return false;
	}
	else{
		printf("Return true\n");
		return true;
	}
}

uint8_t *makeFrames(uint16_t lenghtOfData, uint8_t *dataToSend, uint8_t frameNumber, uint8_t numberOfFrames, uint8_t sequenceNumber){
	
	//static uint8_t frame[22];
	//* static uint8_t *frame = 0;
	//free(frame);
	//* frame = (uint8_t*) calloc(frameSize, sizeof(uint8_t));
	sizeOfFrameToSend=0;
	static uint8_t frame[61];
	for(int i=0; i<frameSize; i++){
		frame[i] = 0;
	}
	//compensate 1 byte for lengthOfData which is used for sequence number. Needed if we have more than 1 frame
	if(frameNumber>0){
		lenghtOfData = lenghtOfData + 2 * frameNumber;
	}

	if(frameNumber+1==numberOfFrames){
		sequenceNumber |= 0x7F; // note that this frame is the last one 
	}
	
	for(int i=0; (i<frameSize && (i-2+frameNumber * frameSize)<(lenghtOfData)); i++){
		//printf("i: %i\n", i);
		//first byte - frame flag 0xFF 
		if(i==0){
			//first byte of frame holds control information: sequence number and flag noting that the the frame is last
			frame[0] = sequenceNumber;
			//printf("FRAME START! %i \n", frame[0]);
		}
		else if(i==frameSize-1 || (i-1+frameNumber * frameSize)==lenghtOfData){
			frame[i] = 0xFF; // mark frame end
			//printf("FRAME END!! i= %i, frame[i]=%i\n", i, frame[i]);
		
		}
		else{
			frame[i] = dataToSend[(i-1)+(frameNumber * (frameSize-2))];
//			printf("KADRAVIMAS: %i\n ", frame[i]);
		}
		sizeOfFrameToSend++;
	}
	//free(frame);
//	for(int i=0; i<frameSize; i++){
//		printf("KADRAVIMAS PABAIGA: %i\n ", frame[i]);
//	}
	return frame;
}

uint8_t *receiveWithStopAndWait(uint8_t *receivedDataSize, bool acceptBroadcastPackets, uint8_t *senderMAC){
	uint8_t sequenceNumber = 0;
	uint8_t receivedSequenceNumber=0;
	static uint8_t assembledData[250];
	
	//static uint8_t *assembledData = 0;
	//free(assembledData);
	//assembledData = (uint8_t*) calloc(1, sizeof(uint8_t)); 
	
//	for(int i =0; i<100; i++){
//		assembledData[i] = 0;
//	}
	uint8_t frameSlot = 0;
	bool isLastFrame = false;
	bool receivedFrame = false;
	// set NULL pointer
	*receivedDataSize=0;
	do{
		//printf("Esu do cikle\n");
		if(RFM69_receiveDone()){
			printf("Target MAC %i\n", RFM69Data.TARGETID );
			*senderMAC = RFM69Data.SENDERID;
			printf("Source MAC %i\n", *senderMAC);
			if(RFM69Data.TARGETID == MYNODEID || (RFM69Data.TARGETID == MACBroadcastAddr && acceptBroadcastPackets)){
				//printf("esu loope\n");
				
				if(RFM69Data.DATA[RFM69Data.DATALEN-1] == 0xFF){
					printf("gautas geras kadras\n");
					//extract data from frame payload
					//frameSlot = 0;

					//calcultae received frame sequence number
					//printf("Pirmas baitas kadro %i\n", RFM69Data.DATA[0]&0x7F);
					receivedSequenceNumber = RFM69Data.DATA[0] & 0x80;
					//printf("Received Sequence number: %i\n", receivedSequenceNumber);
					//printf("My sequence number %i \n", sequenceNumber);
					// if sequence number is the same as expected frames', send ACK for requesting following frame
					if(sequenceNumber==receivedSequenceNumber){
						receivedFrame = true;
						//printf("prasau kito kadro\n");
						
						//uint16_t assembledDataSize = sizeof(assembledData);
						//realloc(assembledData,(RFM69Data.DATALEN-2)+assembledDataSize);
							
						for(int i=1; i<RFM69Data.DATALEN-1; i++){
							assembledData[frameSlot] = RFM69Data.DATA[i];
							//printf("Gautas kadras %i\n", assembledData[frameSlot]);
							frameSlot++;
						}
						sequenceNumber = sequenceNumber ^ 128;
						printf("Sequence number % i\n", sequenceNumber);
						//If not broadcast frame was received send ACK
						if(RFM69Data.TARGETID != MACBroadcastAddr){
							RFM69_sendACK(sequenceNumber);
						}
						if((RFM69Data.DATA[0] & 0x7F) == 127){
							printf("gautas paskutinis kadras\n");
							isLastFrame = true;
						}
						else{
							isLastFrame = false;
						}
					}
					else if(RFM69Data.TARGETID != MACBroadcastAddr){
						//printf("prasau pakartot ta pati kadra\n");
						RFM69_sendACK(sequenceNumber);
						isLastFrame = false;
						//frameSlot=0;
					}
					// if last frame is received abort waiting for other frames; otherwise wait for other frames
					
				}
				else{
					RFM69_sendACK(sequenceNumber);
				}
			}
		}
		//printf("While ciklas\n");
		//printf("Is last frame %i receivedFrame %i\n ", isLastFrame, receivedFrame);
	}
	while(!isLastFrame && receivedFrame);
//	for(int i =0; i<100; i++){
//	printf("Surinkti duomenys %i\n", assembledData[i]);
//	}
	*receivedDataSize = frameSlot;
	//printf("Received data size %i\n", *receivedDataSize);
	return assembledData;
}

//STOP AND WAIT IMPLEMENTATION END
