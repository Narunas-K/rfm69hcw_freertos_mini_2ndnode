/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "RFM69_STM32.h"
#include "datalinkLayer.h"
#include "main.h"
#include "networkLayer.h"
#include "usart.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;
osThreadId myTask02Handle;
osThreadId radioSendTaskHandle;

/* USER CODE BEGIN Variables */

bool initialized = false;

uint8_t data[1] = "2";
uint8_t *dataPointer = 0;
uint8_t receivedData[100];
uint8_t frameData[253];

uint8_t payloadData[100];
//uint16_t myIPaddress[8]={0x0001,0x0010,0x0100,0x1000,0x0002,0x0020,0x0200,0x2000};
uint16_t destinationIPaddress [8] = {0xFD11,0x1111,0x1111,0x2222,0x0000,0x0000,0x0000,0x0002};
uint16_t sizeOfIPpacket = 0;
uint8_t *ptrIPpacket = 0;
uint16_t decodedSourceIPAddress[8];
uint16_t decodedDestinationIPAddress[8];
uint16_t decodedPayloadLength;
uint16_t hopLimit;
uint16_t IPaddressas1[8]={0xFD00,0x0000,0x0001,0x0001,0x0000,0x0000,0x0100,0x0001};
uint16_t IPaddressas2[8]={0xFD00,0x0000,0x0001,0x0001,0x0000,0x0000,0x0100,0x0002};
uint16_t IPaddressas3[8]={0xFD00,0x0000,0x0001,0x0001,0x0000,0x0000,0x0100,0x0003};
uint16_t IPaddressas4[8]={0xFD00,0x0000,0x0001,0x0001,0x0000,0x0000,0x0100,0x0004};
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);
void StartradioSendTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
char Rx_indx, Rx_data[2], Rx_Buffer[100], Transfer_cplt; 
static uint8_t RFM_Transmit_Buffer[100];
uint8_t counter;
uint8_t *pointerToData = 0;
static uint8_t dataArray[100];
static uint8_t nodeNumberArray[1];
uint8_t *pointerToNodeNumber = 0;
uint8_t dataSize;
uint8_t nodeNumberSize =0;
//Interrupt callback routine
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)  
{

		uint8_t i;
    if (huart->Instance == USART3)  //current UART
        {
        if (Rx_indx==0) {for (i=0;i<100;i++) Rx_Buffer[i]=0;}   //clear Rx_Buffer before receiving new data 

        if (Rx_data[0]!=13) //if received data different from ascii 13 (enter)
            {
            Rx_Buffer[Rx_indx++]=Rx_data[0];    //add data to Rx_Buffer
						counter = counter +1;
            }
        else            //if received data = 13
            {
            Rx_indx=0;
            Transfer_cplt=1;//transfer complete, data is ready to read
            }

        HAL_UART_Receive_IT(&huart3, (uint8_t *) Rx_data, 1);   //activate UART receive interrupt every time
        }

}

		uint8_t *uartReceive(uint8_t *count){
			if(Transfer_cplt){
				printf(", message [");
				for(int k = 0; k<100; k++){
					RFM_Transmit_Buffer[k]= (uint8_t) Rx_Buffer[k];
					printf("%c",  Rx_Buffer[k]);
				}
				printf("]\n");
				printf("Counter %i\n", counter);
				*count = counter;
				//RFM69_sendFrame(TONODEID,  RFM_Transmit_Buffer, 50, 0, 0 );
				counter=0;
				Transfer_cplt = 0;
				return RFM_Transmit_Buffer;
			}
			else{
			return 0;
			}
		}

uint16_t *nodeNumberToNodeIP(uint8_t nodeNumber){
	//Here node number is ASCII in DEC
	if(nodeNumber==49){
		return IPaddressas1;
	}
	else if(nodeNumber==50){
		return IPaddressas2;
	}
	else if(nodeNumber==51){
		return IPaddressas3;
	}
	else if(nodeNumber==52){
		return IPaddressas4;
	}
}
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
  RFM69_unselect();
	osDelay(500);
	initialized = RFM69_Initialize(FREQUENCY, MYNODEID, NETWORKID);
	RFM69_setHighPower(true);
	RFM69_promiscuous(true);
	printf("Initialized: %i\n", initialized);
	initializeThisNodeIP();
	initARPtable();
	initRoutingTable();
	
	HAL_Delay(1000);
	
	HAL_UART_Receive_IT(&huart3, (uint8_t *)Rx_data, 1);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 1200);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, StartTask02, osPriorityAboveNormal, 0, 1000);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of radioSendTask */
  osThreadDef(radioSendTask, StartradioSendTask, osPriorityNormal, 0, 1200);
  radioSendTaskHandle = osThreadCreate(osThread(radioSendTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
//		dataPointer = receiveWithStopAndWait();
//		if(*(dataPointer)!=0){
//			for(int i=0; i<100; i++){
//				receivedData[i] = *(dataPointer+i);
//				printf("Gauti duomenys: %i\n", receivedData[i]);
//			}
//		printf("Paskutinis baitas pilnu duomenu\n");
			
//	}
		IPpacketReceived(true);
		osDelay(1);
		
		//printf("Cheching if something was received\n");
		//printf("ar gavau?\n");
//		if(RFM69_receiveDone()){
//			if(RFM69Data.SENDERID == TONODEID){
//				printf("Got DATA! from Node: %i ", RFM69Data.SENDERID);
//				for (uint8_t i = 0; i < RFM69Data.DATALEN; i++){
//					printf("%c", (char)RFM69Data.DATA[i]);
//				}
//				printf(" ");
//				printf("RSSI: %i ", RFM69Data.RSSI);
//				if(RFM69_ACKRequested()){
//					RFM69_sendACK();
//					printf("ACK sent\n");
//				}
//			}
//		}
    
//				printf("Ar gavau?\n");
//   	if(RFM69_receiveDone()){
//			if(RFM69Data.SENDERID == TONODEID){
//				printf("Got DATA! from Node: %i\n ", RFM69Data.SENDERID);
//				for (uint8_t i = 0; i < RFM69Data.DATALEN; i++){
//					printf("%i\n", RFM69Data.DATA[i]);
//					}
//				printf(" ");                                      
//					printf("RSSI: %i\n ", RFM69Data.RSSI);
////				if(RFM69_ACKRequested()){
////					RFM69_sendACK();
////					printf("ACK sent\n");
////				}
//			}
//		}
//		osDelay(500);
		
		
  }
  /* USER CODE END StartDefaultTask */
}

/* StartTask02 function */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  for(;;)
  {
		
		
//		ptrIPpacket=addIPv6header(myIPaddress, destinationIPaddress, 2 ,100, payloadData, &sizeOfIPpacket);
//		for(int i=0; i<sizeOfIPpacket; i++){
//			printf("IP packet byte %i, %i\n", i, *(ptrIPpacket +i));
//		}
//		decodeIPheader(ptrIPpacket, decodedSourceIPAddress, decodedDestinationIPAddress, &decodedPayloadLength, &hopLimit);
//		printf("Decoded Payload Length %i\n Hop limit %i\n", decodedPayloadLength, hopLimit);
//		for(int i=0; i<8; i++){
//			printf(" Decoded Destination IP addres byte %i: %i\n", i, decodedDestinationIPAddress[i]);
//		}
//		for(int i=0; i<8; i++){
//			printf(" Decoded Source IP addres byte %i: %i\n", i, decodedSourceIPAddress[i]);
//		}
		
//	  sendWithStopAndWait(frameData, 100, TONODEID);
//	sendWithStopAndWait(frameData, 100, TONODEID);
	//IPpacketSend(destinationIPaddress, frameData, 50);
		//sendWithStopAndWait(frameData, 88, 255);

		//		IPpacketSend(destinationIPaddress, frameData, 50, 0, 4);
		//sendRREQ(destinationIPaddress);
		//osDelay(700000);
		//determinePrimaryStation();
		
		
		
		
		
		
		node3Test();
		osDelay(900000);
				//printf("Checking if somethin was received\n");
//		printf("Ar gavau?\n");
//   	if(RFM69_receiveDone()){
//			if(RFM69Data.SENDERID == TONODEID){
//				printf("Got DATA! from Node: %i\n ", RFM69Data.SENDERID);
//				for (uint8_t i = 0; i < RFM69Data.DATALEN; i++){
//					printf("%i\n", RFM69Data.DATA[i]);
//					}
//				printf(" ");                                      
//					printf("RSSI: %i\n ", RFM69Data.RSSI);
////				if(RFM69_ACKRequested()){
////					RFM69_sendACK();
////					printf("ACK sent\n");
////				}
//			}
//		}
//		osDelay(500);
		
		
	}
	
  /* USER CODE END StartTask02 */
}

/* StartradioSendTask function */
void StartradioSendTask(void const * argument)
{
  /* USER CODE BEGIN StartradioSendTask */
  /* Infinite loop */
  for(;;)
  {
		
		
	bool nodeDefined = false;
		bool messageDefined = false;

		//printf("default task\n");
		pointerToData = uartReceive(&dataSize);
		
		if(pointerToData!=0 && pointerToData[0] !=57 && pointerToData[0] !=56 && pointerToData[0] !=55){
			printf("%i\n", pointerToData[0]);
			printf("Data size: %i\n", dataSize);
			printf("Message to send:[");
			for(int i=0; i<dataSize; i++){
				dataArray[i] = pointerToData[i];
			}
			for(int i=0; i<dataSize; i++){
				printf("%c",  (char) data[i]);
			}
		printf("] \n");
		//*pointerToData = 0;
		messageDefined = true;
		}
		else if(pointerToData[0]==57){ // in dec is 9
			printf("Updating Node 1 routing table: \n 1. Node3 via Node2 \n 2. Node2 via Node2\n");
			updateRoutingTable(1, IPaddressas3, 0, 0, IPaddressas2, 2, 1000, true);	 // static route to node3 via node 2
			updateRoutingTable(2, IPaddressas2, 0, 0, IPaddressas2, 2, 1000, true);	 // static route to node2 - direct communication
			pointerToData[0]=0x00;
			determinePrimaryStation();
		}
		else if(pointerToData[0]==56){ // in dec is 8
			determinePrimaryStation();
		}
		else if(pointerToData[0]==55){
			printPSinfo();
		}
			if(messageDefined){
			printf("Enter destination node number: ");
			while(!nodeDefined){
				pointerToNodeNumber = uartReceive (&nodeNumberSize);
				if(pointerToNodeNumber!=0){
					nodeDefined = true;
					nodeNumberArray[0] = pointerToNodeNumber[0];
				}
			}
			printf("Node number %c\n ", pointerToNodeNumber[0]);
			IPpacketSend(nodeNumberToNodeIP(nodeNumberArray[0]), dataArray, (uint16_t) dataSize, 0, 4);
		}
		
		
//		IPpacketReceived();
//		sendWithStopAndWait(frameData, 30, TONODEID);
//		osDelay(10000);
//		RFM69_sendFrameWithCSMA(TONODEID, data, 1);
    osDelay(500);
  }
  /* USER CODE END StartradioSendTask */
}

/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
