#include "networkLayer.h"

#include "datalinkLayer.h"
#include "RFM69_STM32.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "rng.h" // random  number generator header file

//NETWORK LAYER GLOBAL VARIABLES BEGIN

// Routing table imlementation
// 16B - Destination IP address
// 4B - Destination sequence number
struct routingTableEntry{
	uint8_t routeEmpty; //
	uint16_t destinationIPaddress[8]; //
	uint32_t destinationSequenceNumber; //
	uint8_t validSequenceNumber; //
	uint8_t flags; //
	uint16_t nextHopIPaddress[8]; //
	uint8_t hopCount;
	uint32_t lifeTime;
};

struct routingTableEntry route1;
struct routingTableEntry route2;
struct routingTableEntry route3;

// ARP table implementation
struct ARPtableEntry{
	uint16_t IPaddress[8];
	uint8_t MACaddress;

};

// ARPtableEntry structures for ARP table entries
struct ARPtableEntry ARPtableEntry0;
struct ARPtableEntry ARPtableEntry1;
struct ARPtableEntry ARPtableEntry2;
struct ARPtableEntry ARPtableEntry3;

// FF02:0000.0000.0000.0000.0000.0000.0001 (or shorter FF02::1) IPv6 multicast address (Will be used as a broadcast address compared to IPv4 255.255.255.255.255)
static uint16_t myIPaddress[8]={0x000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000}; // IP2address
static uint16_t multicastIPaddress[8]={0xFF02,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0001};
static uint16_t PrimaryStationIPaddress[8] = {0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000};
static uint8_t mazgas3Duom [8] = {77, 65, 90, 71, 65, 83, 32, 51};



static uint8_t senderMACaddress=0;

bool thisNodeIsPS = true; // by default node is set to be primary station PS

uint32_t mySequenceNumber = 0; // this node sequence number, according to AODV RFC3561
uint32_t myRREQID = 0; // this node RREQ ID, according to AODV RFC3561
bool RREPtoRREQreceived = false;
uint8_t numberOfTriesToSendRREQ = 0;
//NETWORK LAYER GLOBAL VARIABLES END


//LOCAL FUNCTIONS PROTOTYPES BEGIN
//void updateRoutingTable(uint8_t entry, uint16_t *_destinationIPaddress, uint32_t _destinationSequenceNumber, uint8_t _flags, uint16_t *_nextHopAddress, uint8_t _hopCount, uint32_t _lifeTime, bool isDestSeqNoValid );
void routingTabelRouteDataprint(struct routingTableEntry routeNum);
uint8_t *generateRREQ(uint8_t flags, uint16_t *destinationIPaddress);
//LOCAL FUNCTIONS PROTOTYPES END

//Initializes routing table 
void initRoutingTable(void){
	route1.routeEmpty = 0;
	route2.routeEmpty = 0;
	route3.routeEmpty = 0;
	for(int i=0; i<8; i++){
		route1.destinationIPaddress[i] = 0;
		route2.destinationIPaddress[i] = 0;
		route3.destinationIPaddress[i] = 0;
		
		route1.nextHopIPaddress[i] = 0;
		route2.nextHopIPaddress[i] = 0;
		route3.nextHopIPaddress[i] = 0;
	}
	route1.destinationSequenceNumber = 0;
	route2.destinationSequenceNumber = 0;
	route3.destinationSequenceNumber = 0;
	
	route1.validSequenceNumber = 0;
	route2.validSequenceNumber = 0;
	route3.validSequenceNumber = 0;
	
	route1.flags = 0;
	route2.flags = 0;
	route3.flags = 0;

	route1.hopCount = 0;
	route2.hopCount = 0;
	route3.hopCount = 0;
	
	route1.lifeTime = 0;
	route2.lifeTime = 0;
	route3.lifeTime = 0;

//	updateRoutingTable(1, IPaddress1, 0, 0, IPaddress2, 2, 1000, true);	 // static route to node1 via node2
//	updateRoutingTable(2, IPaddress2, 0, 0, IPaddress2, 2, 1000, true);	 // static route to node2 - direct communication
}


//Initializes ARP table
void  initARPtable(void){
	for(int i=0; i<8; i++){
//		ARPtableEntry0.IPaddress[i] = IPaddress2[i];
//		ARPtableEntry1.IPaddress[i] = IPaddress3[i];
//		ARPtableEntry2.IPaddress[i] = IPaddress4[i];
		ARPtableEntry0.IPaddress[i] = 0;
		ARPtableEntry1.IPaddress[i] = 0;
		ARPtableEntry2.IPaddress[i] = 0;
		ARPtableEntry3.IPaddress[i] = multicastIPaddress[i];
	}
//	ARPtableEntry0.MACaddress = MACaddr2;
//	ARPtableEntry1.MACaddress = MACaddr3;
//	ARPtableEntry2.MACaddress = MACaddr4;
	ARPtableEntry0.MACaddress = 0;
	ARPtableEntry1.MACaddress = 0;
	ARPtableEntry2.MACaddress = 0;
	ARPtableEntry3.MACaddress = MACBroadcastAddr;
}

//*addIPv6header() function adds  IPv6 header to passed payload
//Needs:
//				argument uint16_t *payload
//				argument uint16_t *sourceAddress
//				argument uint16_t *DestinationAddress
//				argument uint16_t hopLimit
//				argument uint16_t payloadLenght
//Returns: 
//				uint16_t* sizeOfIPPacket
//				ptrIPpacket - pointer to first element of IPpacket array
//Please note that:
//				IP header length is 40 bytes
// 				Here  0th LSB bit means LSB bit. Ir we have 1 Byte of data 7th LSB bit is MSB
uint8_t *addIPv6header(uint16_t *sourceAddress, uint16_t *destinationAddress, uint16_t hopLimit, uint16_t payloadLenght, uint8_t *payload){
	//Array which hold whole IP packet
	static uint8_t ptrIPpacket[100];
	
//Assign IPv6 header values
//	uint8_t IPv6header[40]; // 40 bytes IPv6 header size
	uint8_t IPver = 0x06;
	uint8_t trafficClass = 0x00;
	uint8_t flowLabel[3] = {0x00, 0x00, 0x00}; // flowLabel[0] - 19...12 LSB bits, flowLabel[1] -11...4 LSB bits, flowLabel[0] 3...1 LSB bits 
	uint16_t nextHeader = 0x8A; // MANET protocol identifier
	
	//assign first 4Bytes of IPv6 header
	ptrIPpacket[0] = ((IPver << 4) & 0xF0) | ((trafficClass>>4) & 0x0F); //1st byte is IP version 4 MSB bits and 4 LSB bits are trafficClass 4 MSB bits
	ptrIPpacket[1] = ((trafficClass << 4)  & 0xF0) | ((flowLabel[0]>>4) & 0x0F); //2nd byte is trafficClass 4 LSB bits and flowLabel 4 MSB(19..16 LSB bits) bits
	ptrIPpacket[2] =  ((flowLabel[0]<<4) & 0xF0) | ((flowLabel[1]>>4) & 0x0F); //3rd byte is  flowLabel 15..8 LSB bits
	ptrIPpacket[3] = ((flowLabel[1]<<4) & 0xF0) | ((flowLabel[2]>>4) & 0x0F); //4th byte is flowLabel 7..0 LSB bits
//	printf("IP header byte 0: %i\n", IPv6header[0]);
//	printf("IP header byte 1: %i\n", IPv6header[1]);
//	printf("IP header byte 2: %i\n", IPv6header[2]);
//	printf("IP header byte 3: %i\n", IPv6header[3]);
	
	//assign following 4 bytes of IPv6 header
	ptrIPpacket[4] = (payloadLenght >> 4) & 0xFF; //5th byte is payloadLenght 11..4 LSB bits
	ptrIPpacket[5] = ((payloadLenght << 4) & 0xF0) | ((nextHeader >> 6) & 0x0F); //6th byte is payloadLenght 3..0 LSB bits and nextHeader 9..6 LSB bits.
	ptrIPpacket[6] = ((nextHeader << 2) & 0xFC) | ((hopLimit >> 8) & 0x03) ; //7th byte is payload lenght 5..0 LSB bits and 9..8  HopLimit LSB bits
	ptrIPpacket[7] = (hopLimit & 0xFF);
//	printf("IP header byte 4: %i\n", IPv6header[4]);
//	printf("IP header byte 5: %i\n", IPv6header[5]);
//	printf("IP header byte 6: %i\n", IPv6header[6]);
//	printf("IP header byte 7: %i\n", IPv6header[7]);
	
	// assign following 16 bytes of IPv6 header: source IP address
	for(int i=0; i<8; i++){
		//printf("%i\n",8+i*2 );
		//printf("%i\n", 8+(i*2)+1);
		ptrIPpacket[8+i*2] = sourceAddress[i] >> 8;
		ptrIPpacket[8+(i*2)+1] = sourceAddress[i] & 0xFF;
//		printf("Source IP address %i byte: %i\n", i*2, IPv6header[8+i*2]);
//		printf("Source IP address %i byte: %i\n", (i*2)+1, IPv6header[8+(i*2)+1]);
	}
	
	//assign following 16 bytes of IPv6 header: destination IP address
	for(int i=0; i<8; i++){
		ptrIPpacket[24+i*2] = destinationAddress[i] >> 8;
		ptrIPpacket[24+(i*2)+1] = destinationAddress[i] & 0xFF;
//		printf("Destination IP address %i byte: %i\n", i*2, IPv6header[24+i*2]);
//		printf("Destination IP address %i byte: %i\n", (i*2)+1, IPv6header[24+(i*2)+1]);
	}
	//Assign IPv6 header values END  
	//Add IPv6 header to payload
	
	//free(ptrIPpacket);
	//ptrIPpacket = (uint8_t*) malloc(payloadLenght+40*sizeof(uint8_t)); // dinamically allocate array of size of IP packet: IP packet lenght=IPv6_header + payloadLength	
	
//	for(int i=0; i<payloadLenght+40;i++){
//		ptrIPpacket[i] = 0;
//	}
	
//	for(int i=0; i<40; i++){
//		ptrIPpacket[i]= IPv6header[i];
//		printf("IP packet header byte %i: %i\n", i, IPv6header[i]);
//	}
	
	for(int i=40; i<40+payloadLenght; i++){
		ptrIPpacket[i] = payload[i-40];
	}
//Print whole IP packet for debbugging reasons	
	for(int i =0; i<40+payloadLenght; i++){
		//printf("IP packet byte %i: %i\n", i, ptrIPpacket[i]);
	}
//	*sizeOfIPPacket = 40 + payloadLenght;

	return ptrIPpacket;
}

void printPSinfo(void){

	printf("This node is PS: %i\n", thisNodeIsPS);
	for(int i=0; i<8; i++){
		printf("PS station IP address %i byte: %i\n", i, PrimaryStationIPaddress[i]);
	}

}

//decodeIPheader() function decodes IPv6 header from received packet
//Needs:
//				argument *IPpacket
//Returns: 
//				uint16_t array of 8 elements sourceIPaddress 
//				uint16_t array of 8 elements destinationIPaddress 	
//				uint16_t *payloadLength  - IP packet payload length value
//				uint16_t *hopLimit - IP packet hop limit value
void decodeIPheader (uint8_t *IPpacket, uint16_t sourceIPaddress[], uint16_t destinationIPaddress[], uint16_t *payloadLength, uint16_t *hopLimit, uint8_t *IPver){
	
	uint8_t IPv6header[40];
	
	//Header fields variables

	uint8_t trafficClass = 0;
	uint8_t flowLabel[3] = {0, 0, 0}; // flowLabel[0] - 19...12 LSB bits, flowLabel[1] -11...4 LSB bits, flowLabel[0] 3...1 LSB bits 
	uint16_t nextHeader = 0; // MANET protocol identifier
	
	//read IPv6 header from received IP packet
 	for(int i=0; i<40; i++){
		IPv6header[i] = *(IPpacket+i);
	}
	*IPver = IPv6header[0] >> 4; //1st byte of IP header is IP version 4 MSB bits and 4 LSB bits are trafficClass 4 MSB bits
	trafficClass = IPv6header[0] << 4 | IPv6header[1] >> 4; //2nd byte of IP header is trafficClass 4 LSB bits and flowLabel 4 MSB(19..16 LSB bits) bits
	flowLabel[0] = IPv6header[1] << 4 | IPv6header[2] >> 4; //3rd byte of IP header is  flowLabel 15..8 LSB bits
	flowLabel[1] = IPv6header[2] << 4 | IPv6header[3] >> 4; //4th byte of IP header is flowLabel 7..0 LSB bits
	flowLabel[2] = IPv6header[3] << 4; //4th of IP header byte is flowLabel 7..0 LSB bits
	*payloadLength = IPv6header[4] << 4 | IPv6header[5] >> 4; //5th byte of IP header is payloadLenght 11..4 LSB bits
	nextHeader = ((IPv6header[5] << 6)& 0x3C0) | IPv6header[6] >> 2; //6th byte of IP header is payloadLenght 3..0 LSB bits and nextHeader 9..6 LSB bits.
	*hopLimit = ((IPv6header[6] << 8) & 0x300) | IPv6header[7]; //7th byte of IP header is payload lenght 5..0 LSB bits and 9..8  HopLimit LSB bits
	printf("IP version: %i\n", *IPver);
	printf("Traffic class: %i\n", trafficClass);
	printf("Flow label: 1st B: %i, 2nd B: %i, 3rd B %i \n", flowLabel[0], flowLabel[1], flowLabel[2]);
	printf("Payload length: %i\n", *payloadLength);
	printf("Next header: %i\n", nextHeader);
	printf("hop limit: %i\n", *hopLimit);
	
	//convert destination IP address from uint8_t type array to uint16_t type array.
	for(int i=0; i<8; i++){
		sourceIPaddress[i] = ( (uint16_t) IPv6header[8+2*i] <<8 )| (uint16_t) IPv6header[8+(i*2)+1];
		printf("Decoded source IP address %i\n", sourceIPaddress[i]);
	}
	//convert source IP address from uint8_t type array to uint16_t type array.	
	for(int i=0; i<8; i++){
		destinationIPaddress[i] = ( (uint16_t) IPv6header[24+2*i] <<8 )|  (uint16_t) IPv6header[24+(i*2)+1];
		printf("Decoded destination IP address %i\n", destinationIPaddress[i]);
	}
}

//removeIPheader() removes IPv6 header from received packet
//Needs:
//				argument uint8_t *IPpacket
//				argument uint16_t payloadLength
//Returns: 
//				uint8_t *IPpacket pointer to payload array of size IP packet. 
uint8_t *removeIPheader(uint8_t *IPpacket, uint16_t payloadLength){
	//static uint8_t *ptrToPayload;
	//ptrToPayload = (uint8_t*) calloc(48, sizeof(uint8_t));
	
	for(int i=0; i<payloadLength-40 ;i++){
		IPpacket[i] = IPpacket[i+40]; 
		printf("Extracted payload %i byte: %i\n", i, IPpacket[i] );
	}

	return IPpacket;
}

void IPpacketReceived(bool acceptBroadCastPackets){
	uint8_t receivedDataFromL2Size= 0;
	uint8_t *receivedDataFromL2ptr = 0;
	uint8_t *payloadDataptr = 0;
	
	//Variables for data extracted from IP header
	uint16_t extractedSourceIP[8];
	uint16_t extractedDestinationIP[8];
	uint16_t extractedIPpacketPayloadLength = 0;
	uint16_t extractedhopLimit = 0;
	uint8_t  extractedIPver = 0;
	//printf("thisNodeIsPS value: %i\n", thisNodeIsPS);
	//receive data from data link layer
	receivedDataFromL2ptr = receiveWithStopAndWait(&receivedDataFromL2Size, acceptBroadCastPackets, &senderMACaddress);
	
	//check if IP packet was received
	if((receivedDataFromL2Size)!=0){
		//If packet received:
		//1. Read IP header
		decodeIPheader(receivedDataFromL2ptr, extractedSourceIP, extractedDestinationIP, &extractedIPpacketPayloadLength, &extractedhopLimit, &extractedIPver);
		//check if IPv6 packet is received, if not discard the packet
		if(extractedIPver == 0x06){
			//2. Remove IP header (extract payload from IP packet)
			payloadDataptr = removeIPheader(receivedDataFromL2ptr, receivedDataFromL2Size);
			updateARPtable(extractedSourceIP, senderMACaddress);
			//There are 4 possible scenarios what information is in IP packet payload:
			//I. AODV RREQ packet is received (RREQ length is 48 Bytes and 1st Byte is 0x01)
			if(extractedIPpacketPayloadLength == 48 && payloadDataptr[0] == 0x01){
				printf("RREQ packet received\n");
				for(int i=0; i<extractedIPpacketPayloadLength; i++){
					//printf("Gauti duomenys: %i\n", payloadDataptr[i]);
				}
				//printf("The last byte of payload\n");
				proccessRREQ(payloadDataptr, extractedSourceIP, extractedhopLimit);
				//Discard IP packet - free memory
				//free(receivedDataFromL2ptr);
			
			}
			//II.AODV RREP packet is received (RREP length is 44 Bytes and 1st Bte is 0x02)
			else if(extractedIPpacketPayloadLength == 44 && payloadDataptr[0] == 0x02){
				printf("RREP packet received\n");
				processRREP(payloadDataptr, extractedSourceIP, extractedhopLimit);
				//Discard IP packet - free memory
				//free(receivedDataFromL2ptr);
				
			}
			//III. AODV RERR packet is received (RRER length is >= 24+20*k and 1st byte is 0x03)
			else if(extractedIPpacketPayloadLength >= 24&& extractedIPpacketPayloadLength%24 ==0 && payloadDataptr[0] == 0x03){
				printf("RERR packet received\n");
				
				//Discard IP packet - free memory
				//free(receivedDataFromL2ptr);
				
			}
			else if((extractedIPpacketPayloadLength == 17 || extractedIPpacketPayloadLength == 18) && (payloadDataptr[0] == 0x01 || payloadDataptr[0] == 0x02)){
				printf("PS message received\n");
				processPSmessage(payloadDataptr);
			}
			//V. Data packet is received
			else{
				printf("Data packet received\n");
				
				for(int i=0; i<extractedIPpacketPayloadLength; i++){
					//printf("Gauti duomenys: %i\n", payloadDataptr[i]);
				}
				
				processDatapacket(payloadDataptr, receivedDataFromL2Size, extractedSourceIP, extractedDestinationIP, extractedhopLimit);
				//Discard IP packet - free memory
				//free(receivedDataFromL2ptr);
				
			}
		}
		else{
			printf("Corrupted IP packet received, discard the packet\n");
			//Discard IP packet - free memory
			//free(receivedDataFromL2ptr);
		}
			
		}
}

//II.4 Processes received data IP packet. If this node IP address is the same as destination address data is given to application layer, if not IP packet is forwarded to 
void processDatapacket(uint8_t *dataPacket, uint16_t dataLength, uint16_t *sourceIPfromIPheader, uint16_t *destinationIPfromIPheader, uint16_t hopLimit){
	uint16_t newHopLimit = hopLimit - 1;
	if(newHopLimit>0){
		//check if this node IP is the same as destination IP address. If YES, pass the data to application layer, if not forward the packet
		if(compareTwoIPAddresses(destinationIPfromIPheader, myIPaddress)){
			for(int i=0; i<dataLength-40; i++){
				printf("Application data received %i byte: %c\n", i, dataPacket[i]);
			}
		}
		//forward IP packet
		else{
			printf("Forwarding IP packet\n");
			IPpacketSend(destinationIPfromIPheader, dataPacket ,dataLength-40, sourceIPfromIPheader, newHopLimit);
		}
	}
	else{
		printf("IP packet TTL reached 0, discard the packet\n");
	}
}


//II.2 processRREP function - processes RREP message
void processRREP(uint8_t *RREPmessage, uint16_t *sourceIPfromHeader, uint16_t hopLimit){
	bool RREPackRequested;
	uint8_t RREPhopCount;
	uint16_t RREPdestinationIP[8];
	uint32_t RREPdestinationSeqNumber;
	uint16_t RREPoriginatorIP[8];
	uint32_t RREProuteLifeTime;
	uint8_t emptyRouteEntry = 0 ;
	//uint8_t *ptrToRREPpacket = 0;
	struct routingTableEntry *ptrRouteEntry;
	bool routeExist = false;
	uint8_t entryNum = 0;
	uint8_t newHopCount;
	extractRREP(RREPmessage, &RREPackRequested, RREPdestinationIP ,&RREPhopCount, &RREPdestinationSeqNumber, RREPoriginatorIP, &RREProuteLifeTime);
	
	//DEBUG: print extracted RREP data
	for(int i=0; i<8; i++){
		printf("ProccesRREP(): destination IP address %i byte: %i\n", i, RREPdestinationIP[i]);
	}
	for(int i=0; i<8; i++){
		printf("ProccesRREP(): originator IP address %i byte: %i\n", i, RREPoriginatorIP[i]);
	}
	
	printf("ProccesRREP() RREPackRequested: %i\n", RREPackRequested);
	printf("ProccesRREP() RREPhopCount: %i\n", RREPhopCount);
	printf("ProccesRREP() Destination seq. no.: %i\n", RREPdestinationSeqNumber);
	printf("ProccesRREP() RREProuteLifeTime: %i\n", 	RREProuteLifeTime);
	
	//check if the route to source node exists, if not create route to node which forwarded RREP packet, but without valid sequence number
	routeExist = routeExists(sourceIPfromHeader, &entryNum);
	if(!(routeExist && (entryNum !=0))){
		printf("processRREP() 2\n");
		emptyRouteEntry = routingTableEntryNumToWhichEmptyEntryToWrite();
		// Update routing table
			updateRoutingTable(emptyRouteEntry, RREPdestinationIP, 0, 0, sourceIPfromHeader, 1, 1000, false);
	}
	newHopCount = RREPhopCount+1;
	routeExist = routeExists(RREPdestinationIP, &entryNum);
	
	//check if the route to destination node exists. If YES, update route entry, if NO create new entry according to received RREP and IP header data
	if(entryNum!=0){
			switch(entryNum){
			case 1:
				ptrRouteEntry = &route1;
				break;
			case 2:
				ptrRouteEntry = &route2;
				break;
			case 3:
				ptrRouteEntry = &route3;
				break;
		}
		//check if a route entry to destination node needs to be updated or not
		if(ptrRouteEntry->validSequenceNumber == 0 || RREPdestinationSeqNumber > ptrRouteEntry->destinationSequenceNumber || newHopCount < ptrRouteEntry->hopCount){
			updateRoutingTable(entryNum, RREPdestinationIP, RREPdestinationSeqNumber, 0, sourceIPfromHeader, newHopCount, RREProuteLifeTime, true);
			printf("processRREP() 3\n");
		}
	}
	//NO, create forward route entry to destination node
	else{
		emptyRouteEntry = routingTableEntryNumToWhichEmptyEntryToWrite();
		updateRoutingTable(emptyRouteEntry, RREPdestinationIP, RREPdestinationSeqNumber, 0, sourceIPfromHeader, newHopCount, RREProuteLifeTime, true);
		printf("processRREP() 4\n");
	}
	
	//check if RREPoriginatorIP is the same as myIPaddress. If not , unicast RREP to next hopto originator node
	if(!compareTwoIPAddresses(RREPoriginatorIP, myIPaddress)){
		printf("processRREP() 5\n");
		RREPmessage[3] = newHopCount;
		hopLimit--;
		if(hopLimit>0){
			IPpacketSend(RREPoriginatorIP, RREPmessage, 44, 0,hopLimit); // fixed bug here, changed RREPdestinationIP to RREPoriginatorIP, logical mistake.
		}
		else{
			printf("RREP packet TTL is 0, disscard the packet\n");
		}
		
	}
	else{
		RREPtoRREQreceived = true;
	}
	printf("processRREP() 6: printing all routing table entries\n");
	
	printf("Route1 entry: \n");
	routingTabelRouteDataprint(route1);
	printf("Route2 entry: \n");
	routingTabelRouteDataprint(route2);
	printf("Route3 entry: \n");
	routingTabelRouteDataprint(route3);
}

//returns to which routing table entry to write if needed wmpty routing table entry
uint8_t routingTableEntryNumToWhichEmptyEntryToWrite(void){
	uint8_t emptyRouteEntries = 0;
	uint8_t routeEntryToWrite = 0;
	emptyRoutingTableEntries(&emptyRouteEntries);
		
		if(emptyRouteEntries & 0x01){
		// write to routing table first entry
			routeEntryToWrite = 1;
		}
		else if(emptyRouteEntries & 0x02){
		// write to routing table second entry
			routeEntryToWrite = 2;
		}
		else if(emptyRouteEntries & 0x04){
		// write to routing table third entry
			routeEntryToWrite = 3;
		}
	return routeEntryToWrite;
}

//II.1 processRREQ function - processes RREQ message 
void proccessRREQ(uint8_t * RREQmessage, uint16_t *sourceIPfromIPheader, uint16_t hopLimit){
	//bool doRouteExist = false;
	//uint8_t RouteEntryNumber = 0;
	//uint8_t emptyRouteEntries = 0;
	uint8_t DestinationRouteEntry = 0;
	bool doRouteToDestinationExist = false;
	
	uint8_t  RREQflags = 0;
	uint16_t RREQdestinationIPaddress[8];
	uint8_t  RREQhopCount = 0;
	uint32_t RREQID = 0;
	uint32_t RREQdestinationSeqNo = 0;
	uint16_t RREQoriginatorIPaddress[8];
	uint32_t RREQoriginatorSeqNo = 0;
	uint8_t *RREQIPpacketPtr = 0;
	static uint8_t RREQIPpacket[88];
	bool returned=false;
	
	
// extract data from RREQ packet
	extractRREQ(RREQmessage, &RREQflags, RREQdestinationIPaddress, &RREQhopCount, &RREQID, &RREQdestinationSeqNo, RREQoriginatorIPaddress, &RREQoriginatorSeqNo);

	//DEBUG: print extracted RREQ data
	for(int i=0; i<8; i++){
		printf("Procces RREQ: destination IP address %i byte: %i\n", i, RREQdestinationIPaddress[i]);
	}
	for(int i=0; i<8; i++){
		printf("Procces RREQ: originator IP address %i byte: %i\n", i, RREQoriginatorIPaddress[i]);
	}
	printf("Procces RREQ Flags: %i\n", RREQflags);
	printf("Procces RREQHop count: %i\n", RREQhopCount);
	printf("Procces RREQ RREQ ID: %i\n", RREQID);
	printf("Procces RREQ Destination seq. no.: %i\n", RREQdestinationSeqNo);
	printf("Procces RREQ Originator seq. no.: %i\n", 	RREQoriginatorSeqNo);
	
	UpdateRouteTableAfterRREQreceived(RREQoriginatorIPaddress, sourceIPfromIPheader, RREQoriginatorSeqNo,  RREQflags, RREQhopCount );
	
	printf("Route1 entry: \n");
	routingTabelRouteDataprint(route1);
	printf("Route2 entry: \n");
	routingTabelRouteDataprint(route2);
	printf("Route3 entry: \n");
	routingTabelRouteDataprint(route3);
	
	//check if RREQ is generated by this node. If yes, discard the packet, if not process the packet
	if(RREQID != myRREQID && !compareTwoIPAddresses(RREQoriginatorIPaddress, myIPaddress)){
		printf("proccessRREQ 1\n");
		//check if destination IP is the same as myIP. If yes, generate RREP packet, if not check if there
		//is a route to destination in routing table and send RREP, if not broadcast RREQ
		if(compareTwoIPAddresses(RREQdestinationIPaddress, myIPaddress)){
			printf("proccessRREQ 2\n");
			printf("DestinationIP is the same as myIP\n");
			// myIP is the same as destinationIP, generate RREP II.1.1
			generateRREPDestinationNode(myIPaddress, RREQoriginatorIPaddress, mySequenceNumber, 1000);
		}
		else{
			printf("proccessRREQ 3\n");
			//Check if there is a route in routing table to the destination node (RREQdestinationIPaddress)
			doRouteToDestinationExist=routeExists(RREQdestinationIPaddress, &DestinationRouteEntry);
			if(doRouteToDestinationExist && DestinationRouteEntry != 0){
				printf("proccessRREQ 4\n");
				//Send RREP (GenerateRREPintermediateNode) //////////////////////////////////////////////////////////////////////////////////////////////////////
				
			}
			else{
				printf("proccessRREQ 5\n");
				//Update RREQ and broadcast it
				RREQhopCount++;
				hopLimit--;
				//if hopLimit<0 -> discard packet
				if(!(hopLimit>0)){
					printf("proccessRREQ 6\n");
					printf("IP packet HOP limit is reached. Discard the packet\n");
					//free(RREQmessage);
				}
				else{
					printf("proccessRREQ 7\n");
					RREQmessage[3] = RREQhopCount;
					//print if debugging is needed
					for(int i=0; i<48; i++){
						//printf("RREQ broadcasted message %i byte: %i\n", i, RREQmessage[i]);
					}
					static uint16_t multicastIPaddr[8]={0xFF02,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0001};
					RREQIPpacketPtr = addIPv6header(myIPaddress, multicastIPaddr, hopLimit, 48, RREQmessage);
					
					for(int i=0; i<88; i++){
						RREQIPpacket[i] = RREQIPpacketPtr[i];
						//printf("RREQ IP packet %i byte: %i\n", i, RREQIPpacket[i]);
					}
					//Wait some time to avoid sending broadcast frame at the same time as othe nodes
					HAL_Delay(400);
					//broadcast RREQ packet
					returned = sendWithStopAndWait(RREQIPpacket, 88, 255);
					//free(RREQIPpacket);
					//free(RREQmessage);
				}
				
			}
	}
	}
	else{
		printf("proccessRREQ 8\n");
		//free(RREQmessage);
	}
}

void generateRREPDestinationNode(uint16_t *destIP, uint16_t *originatorIP, uint32_t destSeqNo ,uint32_t routeLifeTime){
	printf("generateRREPDestinationNode()\n");
	uint8_t *ptrRREPmessage = 0;
	bool requireACK = false; 
	//check if mySequenceNumber==destSeqNo-1, if yes mySequenceNumber++, if not leave mySequenceNumeber as is. According to AODV RFC3561
	if(mySequenceNumber == (destSeqNo-1)){
		mySequenceNumber++;
	}
	//generate RREP message
	ptrRREPmessage = generateRREP(destIP, originatorIP, mySequenceNumber, requireACK, 1000);
	
	// unicast RREP to originator node. Payload length is RREP size - 44B 
	IPpacketSend(originatorIP, ptrRREPmessage, 44, 0, 4);

}

//extacts data from RREP message
void extractRREP(uint8_t *RREPmessage, bool *ACKrequested, uint16_t *destIP, uint8_t *hopCount, uint32_t *destSeqNo, uint16_t *originatorIP, uint32_t *lifeTime){
	*ACKrequested = RREPmessage[1]>>6 & 0x01;
	*hopCount =RREPmessage[3];
	printf("extactRREP()\n");
	//extract destination IP address
	for(int i=0; i<8; i++){
		destIP[i] = RREPmessage[4+i*2] << 8 | RREPmessage[4+(i*2)+1];
		printf("RREP Dest IP address %i byte: %i\n", i, destIP[i]);
	}
	
	//extract originator IP address
	for(int i=0; i<8; i++){
		originatorIP[i] = RREPmessage[24+i*2] << 8 | RREPmessage[24+(i*2)+1];
		printf("RREP originatorIP IP address %i byte: %i\n", i, originatorIP[i]);
	}
	
	//extract destination sequence number
	*destSeqNo = RREPmessage[20] << 24 | RREPmessage[21] << 16 | RREPmessage[22] <<8 | RREPmessage[23];
	
	//extract lifetime of route
	*lifeTime  = RREPmessage[40] << 24 | RREPmessage[41] << 16 | RREPmessage[42] << 8 | RREPmessage[43];
}

uint8_t *generateRREP(uint16_t *destIP, uint16_t *originatorIP, uint32_t destSeqNo, bool requireACK, uint32_t routeLifeTime){
	printf("generateRREP()\n");
	static uint8_t RREPmessage[44];
	uint8_t hopCount = 0; // set to 0 to when generated. If node forwards RREP packet this field is incremented by 1 
	RREPmessage[0] = 0x02; // RREP message type (8 bits) is 0x02

	RREPmessage[1] = requireACK<<6 & 0xC0; // Here 1st and 2nd MSB are route repair and request ACK flags, other 6 LSB are 0's (reserved bits)
	RREPmessage[2] = 0x00; //First 3 MSB bits are reserved, 5 LSB bist are Prefix size, here We are not implementing this feeature, so set to 0s
	RREPmessage[3] = hopCount;
	//Assign following 16 bytes of destination IP address
	for(int i=0; i<8; i++){
		RREPmessage[4+i*2] = destIP[i] >> 8;
		RREPmessage[4+(i*2)+1] = destIP[i] & 0xFF;
//		printf("RREP Dest IP address %i byte: %i\n", i*2, RREPmessage[4+i*2]);
//		printf("RREP Dest IP address %i byte: %i\n", (i*2)+1, RREPmessage[4+(i*2)+1]);
	}
	
	//Assign Destination sequence number
	RREPmessage[20] = (destSeqNo >>24);
	RREPmessage[21] = (destSeqNo >>16); 
	RREPmessage[22] = (destSeqNo >>8); 
	RREPmessage[23] =  destSeqNo & 0xFF;
	
	//Assing  following 16 bytes of originator(node which generated RREQ) IPv6 address
	for(int i=0; i<8; i++){
		RREPmessage[24+i*2] = originatorIP[i] >> 8;
		RREPmessage[24+(i*2)+1] = originatorIP[i] & 0xFF;
//		printf("Originator IP address %i byte: %i\n", i*2, RREPmessage[24+i*2]);
//		printf("Originator IP address %i byte: %i\n", (i*2)+1, RREPmessage[24+(i*2)+1]);
	}
	
	RREPmessage[40] = (routeLifeTime >>24);
	RREPmessage[41] = (routeLifeTime >>16); 
	RREPmessage[42] = (routeLifeTime >>8); 
	RREPmessage[43] =  routeLifeTime & 0xFF;
	
	for(int i=0; i<44; i++){
		//printf("RREP message byte %i: %i\n",i, RREPmessage[i]);
	}
	
	return RREPmessage;
}




//G.1. Updates routing table after RREQ packet is received
void UpdateRouteTableAfterRREQreceived(uint16_t *RREQoriginatorIPaddress, uint16_t *sourceIPfromIPheader, uint32_t RREQoriginatorSeqNo, uint8_t RREQflags, uint8_t RREQhopCount ){
	
	printf("UpdateRouteTableAfterRREQreceived \n");
	
	struct routingTableEntry *ptrRouteEntry;
	bool doRouteExist = false;
	uint8_t RouteEntryNumber = 0;
	uint8_t emptyRouteEntries = 0;
// UPDATE routing table
// 1. Check if in routing table there is a route to the originator of RREQ. If yes, check if it is valid
	doRouteExist = routeExists(RREQoriginatorIPaddress, &RouteEntryNumber);
	// There is no entry of route to originator node
	if(doRouteExist == false && RouteEntryNumber ==0){
		printf("UpdateRouteTableAfterRREQreceived 1\n");
		// figure out to which routing table entry write the new route
		emptyRoutingTableEntries(&emptyRouteEntries);
		// write to routing table entry number 1
		if((emptyRouteEntries & 0x01) == 0x01){
			// update route entry route1
			updateRoutingTable(1, RREQoriginatorIPaddress, RREQoriginatorSeqNo, RREQflags, sourceIPfromIPheader, RREQhopCount + 1, 1000, true);
			printf("UpdateRouteTableAfterRREQreceived 2\n");
		}
		// write to routing table entry number 2
		else if((emptyRouteEntries & 0x02) == 0x02){
			// update route entry route2
		 updateRoutingTable(2, RREQoriginatorIPaddress, RREQoriginatorSeqNo, RREQflags, sourceIPfromIPheader, RREQhopCount + 1, 1000, true);
			printf("UpdateRouteTableAfterRREQreceived 3\n");
		}
		// write to routing table entry number 3
		else if((emptyRouteEntries & 0x04) == 0x04){
			// update route entry route2
			updateRoutingTable(3, RREQoriginatorIPaddress, RREQoriginatorSeqNo, RREQflags, sourceIPfromIPheader, RREQhopCount + 1, 1000, true);
			printf("UpdateRouteTableAfterRREQreceived4\n");
		}
	}
	// There is an entry of route to originator node, but it is expired. Write renewed route date 
	else if(doRouteExist == false && RouteEntryNumber !=0){
		// update route entry route2
		updateRoutingTable(RouteEntryNumber, RREQoriginatorIPaddress, RREQoriginatorSeqNo, RREQflags, sourceIPfromIPheader, RREQhopCount + 1, 1000, true);
		printf("UpdateRouteTableAfterRREQreceived 5\n");
	}
	// There is an entry of valid route to originator node
	else if(doRouteExist == true && RouteEntryNumber !=0){
		printf("UpdateRouteTableAfterRREQreceived 6\n");
		//check if current destination sequence number of originator node is newer or not. If not, write the most recent known value (RREQoriginatorSeqNo)
		switch(RouteEntryNumber){
			case 1:
				ptrRouteEntry = &route1;
				break;
			case 2:
				ptrRouteEntry = &route2;
				break;
			case 3:
				ptrRouteEntry = &route3;
				break;
		}
		if(ptrRouteEntry->destinationSequenceNumber < RREQoriginatorSeqNo){
			ptrRouteEntry -> destinationSequenceNumber = RREQoriginatorSeqNo;
			ptrRouteEntry -> validSequenceNumber = 1;
			//Also update next hop information
			printf("Updated next hop address with new destination sequence number\n");
			for(int i =0; i<8; i++){
				ptrRouteEntry ->nextHopIPaddress[i] = sourceIPfromIPheader[i];
				printf("Next hop IP address %i byte: %i\n",i ,ptrRouteEntry ->nextHopIPaddress[i]);
			}
		
			printf("UpdateRouteTableAfterRREQreceived 7\n");
		}
	}
	
}


void extractRREQ(uint8_t *RREQmessage, uint8_t *flags, uint16_t *destinationIPaddr, uint8_t *hopCount, uint32_t *RREQID, 
										uint32_t *destinationSeqNo, uint16_t *originatorIPaddr, uint32_t *originatorSeqNo){
											
	*flags = RREQmessage[1];
	*hopCount = RREQmessage[3];
	
	*RREQID = RREQmessage[4] <<24 | RREQmessage[5] <<16 | RREQmessage[6] <<8 |RREQmessage[7];
	
	//assign destination IP address
	for(int i=0; i<8; i++){
		destinationIPaddr[i] = RREQmessage[8+i*2] << 8 | RREQmessage[8+(i*2)+1];
		printf("Destination IP address %i byte: %i\n", i, destinationIPaddr[i]);
	}
	
	*destinationSeqNo = RREQmessage[24] <<24 | RREQmessage[25] << 16  | RREQmessage[26] << 8 | RREQmessage[27];
		
	for(int i=0; i<8; i++){
		originatorIPaddr[i] = RREQmessage[28+i*2] << 8 | RREQmessage[28+(i*2)+1];
		printf("Originator IP address %i byte: %i\n", i, originatorIPaddr[i]);
	}
	
	*originatorSeqNo = RREQmessage[44] <<24 | RREQmessage[45] << 16  | RREQmessage[46] << 8 | RREQmessage[47];
	
	//print extracted data for debugging reasons
	printf("Flags: %i\n", *flags);
	printf("Hop count: %i\n", *hopCount);
	printf("RREQ ID: %i\n", *RREQID);
	printf("Destination seq. no.: %i\n", *destinationSeqNo);
	printf("Originator seq. no.: %i\n", *originatorSeqNo);
										
}



bool compareARPIP(uint16_t *destinationIPaddress, struct ARPtableEntry entry ){
		for(int i=0; i<8; i++){
			if(entry.IPaddress[i]!=destinationIPaddress[i]){
				return false;
			}
		}
		//if all bytes of IP addres are the same
		return true;
}

uint8_t resolveMACaddress(uint16_t *destinationIPaddress){
	bool ARPentry0 = false;
	bool ARPentry1 = false;
	bool ARPentry2 = false;
	bool ARPentry3 = false;
	
	ARPentry0 = compareARPIP(destinationIPaddress, ARPtableEntry0);
	ARPentry1 = compareARPIP(destinationIPaddress, ARPtableEntry1);
	ARPentry2 = compareARPIP(destinationIPaddress, ARPtableEntry2);
	ARPentry3 = compareARPIP(destinationIPaddress, ARPtableEntry3);
	
	if(ARPentry0){
		return ARPtableEntry0.MACaddress;
	}
	else if(ARPentry1){
		return ARPtableEntry1.MACaddress;
	}
	else if(ARPentry2){
		return ARPtableEntry2.MACaddress;
	}
	else if(ARPentry3){
		return ARPtableEntry3.MACaddress;
	}
}

void markRouteAsinvalid (uint8_t entry, bool isDestSeqNoValid){
	uint8_t validSeqNo;
	if(isDestSeqNoValid){
		validSeqNo = 1;
	}
	else{
		validSeqNo = 0;
	}
	struct routingTableEntry *ptr;
	switch(entry){
				case 1:
					ptr = &route1;
					break;
				case 2:
					ptr = &route2;
					break;
				case 3:
					ptr = &route3;
					break;
			}
		ptr->validSequenceNumber = validSeqNo;
}

// Function used to send data packets using IP 
void IPpacketSend(uint16_t *destinationIPaddress, uint8_t *payload, uint16_t payloadLength, uint16_t *sourceIPAddress, uint16_t hopLimit){
	numberOfTriesToSendRREQ = 0;
	RREPtoRREQreceived = false;
	bool multicast = false;
	uint32_t timeBegin;
	uint32_t timeEnd;
	
	bool doRouteExist = false;
	bool packetSent = false;
	uint8_t routingTableEntryNumber; // if 0 - routing table is empty
	uint8_t sizeOfIPpacket = 0;
	uint8_t *ptrIPpacket=0;
	uint8_t resolvedMACaddr = 0;
	//Check if route exists. If exists send packet to next hop stated in routing table
	doRouteExist = routeExists(destinationIPaddress, &routingTableEntryNumber);
	printf("Route exists %i\n", doRouteExist);
	printf("Routing table entry number %i\n", routingTableEntryNumber);
	printf("Printing whole routing table: \n");
	printf("Route 1:\n");
	routingTabelRouteDataprint(route1);
	printf("Route 2:\n");
	routingTabelRouteDataprint(route2);
	printf("Route 3:\n");
	routingTabelRouteDataprint(route3);
	if(destinationIPaddress[0]==65282){
		multicast = true;
	}
	else{
		multicast = false;
	}
	if(doRouteExist || multicast){
		sizeOfIPpacket = payloadLength + 40;
		// add IP header. If sourceIPAddress is 0, source IP is this node IP address, if not source IP is IP address passed to IPpacketSend().
		if(sourceIPAddress!=0){
			//printf("source IP is not NULL\n");
			ptrIPpacket = addIPv6header(sourceIPAddress, destinationIPaddress, hopLimit, payloadLength,  payload);
		}
		else{
			//printf("source IP is NULL, add myIPaddress\n");
			ptrIPpacket = addIPv6header(myIPaddress, destinationIPaddress, hopLimit, payloadLength,  payload);
		}
		
		for(int i=0; i<sizeOfIPpacket; i++){
			//printf("With IP header IP packet byte %i: %i\n",i, ptrIPpacket[i]);
		}
		//Resolve MAC address and send packet
			switch(routingTableEntryNumber){
				case 0:
					resolvedMACaddr = 255;
					break;
				case 1:
					resolvedMACaddr = resolveMACaddress(route1.nextHopIPaddress);
					break;
				case 2:
					resolvedMACaddr = resolveMACaddress(route2.nextHopIPaddress);
					break;
				case 3:
					resolvedMACaddr = resolveMACaddress(route3.nextHopIPaddress);
					break;
			}
		printf("Resolved MAC address: %i\n ", resolvedMACaddr);
		packetSent = sendWithStopAndWait(ptrIPpacket, sizeOfIPpacket, resolvedMACaddr);
		//release allocated memmory in addIPheader function
		//free(ptrIPpacket);
		//ptrIPpacket = 0;
		if(packetSent == false){
			printf("Host is unreachable\n");
			markRouteAsinvalid(routingTableEntryNumber, false);
			printf("Route %i marked as invalid\n", routingTableEntryNumber);
		}
	}
	//Route does not exist, initiate route finding procedure
	else{
		while(!RREPtoRREQreceived && (numberOfTriesToSendRREQ < 5)){
			sendRREQ(destinationIPaddress);
			HAL_Delay(500);
			timeBegin = millis();
			do{
				HAL_Delay(100);
				IPpacketReceived(false); // wait for RREP packet, disable receiving broadcast packets
				timeEnd= millis();
			}while(timeEnd-timeBegin <5000);
			numberOfTriesToSendRREQ++;
			printf("Number of tries to send RREQ %i\n", numberOfTriesToSendRREQ);
			printf("!RREPtoRREQreceived %i\n", !RREPtoRREQreceived);
		}
		if(RREPtoRREQreceived){
				sizeOfIPpacket = payloadLength + 40;
		// add IP header. If sourceIPAddress is 0, source IP is this node IP address, if not source IP is IP address passed to IPpacketSend().
			if(sourceIPAddress!=0){
				//printf("source IP is not NULL\n");
				ptrIPpacket = addIPv6header(sourceIPAddress, destinationIPaddress, hopLimit, payloadLength,  payload);
			}
			else{
				//printf("source IP is NULL, add myIPaddress\n");
				ptrIPpacket = addIPv6header(myIPaddress, destinationIPaddress, hopLimit, payloadLength,  payload);
			}
			
			for(int i=0; i<sizeOfIPpacket; i++){
				//printf("With IP header IP packet byte %i: %i\n",i, ptrIPpacket[i]);
			}
			//send packet
			resolvedMACaddr = resolveMACaddress(destinationIPaddress);
			packetSent = sendWithStopAndWait(ptrIPpacket, sizeOfIPpacket, resolvedMACaddr);
			if(packetSent == false){
				printf("Host is unreachable\n");
		}
			}
	}

}

bool compareDestinationIP(uint16_t *destinationIPaddress, struct routingTableEntry route ){
		for(int i=0; i<8; i++){
			if(route.destinationIPaddress[i]!=destinationIPaddress[i]){
				return false;
			}
		}
		//if all bytes of IP addres are the same
		return true;
}

bool compareTwoIPAddresses(uint16_t *firstIPaddress, uint16_t *secondIPaddress){
		for(int i=0; i<8; i++){
			if(firstIPaddress[i]!=secondIPaddress[i]){
				return false;
			}
		}
		//if all bytes of IP addres are the same
		return true;
}

uint8_t routingTableEntryNumber(bool destIPexists1, bool destIPexists2, bool destIPexists3){
	// assumption that we could only have one route to destination 
	if(destIPexists1){
	return 1;
	}
	else if(destIPexists2){
	return 2;
	}
	else if(destIPexists3){
	return 3;
	}
	return 0;
}

//Returns routing table entries number which are empty
void emptyRoutingTableEntries( uint8_t *emptyEntriesNumber){
		*emptyEntriesNumber = 0;
	// If emptyEntriesNumber = 0b00000001 -> means first entry is empty, if 0b00000010 -> entry number 2 is empty, if 0b00000011 -> entry 1 and 2 are empty and etc. 
		if(route1.routeEmpty == 0){
			*emptyEntriesNumber |= 0x01; 
		}
		if(route2.routeEmpty == 0){
			*emptyEntriesNumber |= 0x01<<2; 
		}
		if(route3.routeEmpty == 0){
			*emptyEntriesNumber |= 0x01<<3; 
		}
}

//Checks if a oute to destinationIPaddress exists, returns True of False. Also returns route entry number in routing table if routeExists
bool routeExists(uint16_t *destinationIPaddress, uint8_t *EntryNumber){
	//check if routing table is not empty
	
	bool destIPexists1 =false, destIPexists2 = false, destIPexists3=false;
	*EntryNumber = 0;
	struct routingTableEntry *ptr;
	//printf("RouteExists() 1\n");
	if((route1.routeEmpty||route2.routeEmpty || route3.routeEmpty) !=0){
		printf("RouteExists() 2\n");
		destIPexists1 = compareDestinationIP(destinationIPaddress, route1);
		destIPexists2 = compareDestinationIP(destinationIPaddress, route2);
		destIPexists3 = compareDestinationIP(destinationIPaddress, route3);
		*EntryNumber = routingTableEntryNumber(destIPexists1, destIPexists2, destIPexists3);
		if(destIPexists1 || destIPexists2 || destIPexists3){
			//printf("RouteExists()  3\n");
			switch(*EntryNumber){
				case 1:
					ptr = &route1;
					break;
				case 2:
					ptr = &route2;
					break;
				case 3:
					ptr = &route3;
					break;
			}
			if(ptr ->validSequenceNumber != false){
					//printf("RouteExists() 4\n");
				return true;
			}
			else{
					//printf("RouteExists() 5\n");
			// route is not valid
			return false;
			}
			
		}
		// table does not have an entry of destination IP
		else{
				//printf("RouteExists() 6\n");
			return false;
		}
	}
	//table is empty, return FALSE - route does not exist
	else{
			//printf("Route Exists  7\n");
		return false;
	}
}

void updateRoutingTable(uint8_t entry, uint16_t *_destinationIPaddress, uint32_t _destinationSequenceNumber, uint8_t _flags, uint16_t *_nextHopAddress, uint8_t _hopCount, uint32_t _lifeTime, bool isDestSeqNoValid ){
	uint8_t validSeqNo;
	if(isDestSeqNoValid){
		validSeqNo = 1;
	}
	else{
		validSeqNo = 0;
	}
	
	struct routingTableEntry *ptr;
	switch(entry){
				case 1:
					ptr = &route1;
					break;
				case 2:
					ptr = &route2;
					break;
				case 3:
					ptr = &route3;
					break;
			}
	ptr->routeEmpty = 1;
	
	for(int i=0; i<8; i++){
		ptr->destinationIPaddress[i] = _destinationIPaddress[i];
		ptr->nextHopIPaddress[i] = _nextHopAddress[i];
	}
	ptr->destinationSequenceNumber = _destinationSequenceNumber;
	ptr->validSequenceNumber = validSeqNo;
	ptr->flags = _flags;
	ptr->hopCount = _hopCount;
	ptr->lifeTime = _lifeTime;
}

//sends RREQ (Route Request) packet
void sendRREQ(uint16_t *destinationIPaddress){
	//Before sending RREQ packet increase MySequenceNumber and myRREQID
	mySequenceNumber++;
	myRREQID++;
	uint8_t *RREQpacket;
	uint8_t *IPpacket;
	static uint16_t multicastIPaddr[8]={0x1111,0x2222,0x3333,0x4444,0x5555,0x6666,0x7777,0x8888};
	
	RREQpacket = generateRREQ(31, destinationIPaddress);
//	for(int i=0; i<48; i++){
//		printf("RREQ message byte %i: %i\n",i, RREQpacket[i]);
//	}
	IPpacket = addIPv6header(myIPaddress, multicastIPaddr, 4, 48, RREQpacket);
	
	for(int i=0; i<88; i++){
		//printf("IP packet byte %i: %i\n",i, IPpacket[i]);
	}
	
	sendWithStopAndWait(IPpacket, 88, MACBroadcastAddr);// IP packet size is 48+40=88, 255 MAC broadCast address
	//release allocated memmory in addIPheader function
	//free(IPpacket);
}

//*generateRREQ() function generates RREQ packet
//Needs:
//				argument uint8_t flags (MSB - JRGDU000 - LSB)
//				argument uint16_t *destinationIPAddress
//Returns: 
//				uint8_t* RREQmessage
//Please note that:
//				RREQmessage is always 48 Bytes
// 				Here  0th LSB bit means LSB bit. If we have 1 Byte of data 7th LSB bit is MSB
uint8_t *generateRREQ(uint8_t flags, uint16_t *destinationIPaddress){
	static uint8_t RREQmessage[48];
	uint8_t hopCount = 0; // set to 0 to when generated. If node forwards RREQ packet this field is incremented by 1 
	//uint8_t flags constits of: MSB - JRGDU000 LSB -
	RREQmessage[0] = 0x01; // RREQ message type (8 bits) is 1
	RREQmessage[1] = flags; // Here 5 MSB are flags bits and 3 LSB are 0's (reserved bits)
	RREQmessage[2] = 0x00; // Reserved 8 bits
	RREQmessage[3] = hopCount;
	//Assign RREQ ID
	RREQmessage[4] = (myRREQID >>24);
	RREQmessage[5] = (myRREQID >>16); 
	RREQmessage[6] = (myRREQID >>8); 
	RREQmessage[7] =  myRREQID & 0xFF;
	
	// assign following 16 bytes of  destination IP address
	for(int i=0; i<8; i++){
		RREQmessage[8+i*2] = destinationIPaddress[i] >> 8;
		RREQmessage[8+(i*2)+1] = destinationIPaddress[i] & 0xFF;
//		printf("Source IP address %i byte: %i\n", i*2, IPv6header[8+i*2]);
//		printf("Source IP address %i byte: %i\n", (i*2)+1, IPv6header[8+(i*2)+1]);
	}
	
	// Assume that we do not know destination sequence number
	RREQmessage[24] = 0;
	RREQmessage[25] = 0; 
	RREQmessage[26] = 0; 
	RREQmessage[27] = 0;
	
	//Assing  following 16 bytes of this node (originator) IPv6 address
	for(int i=0; i<8; i++){
		RREQmessage[28+i*2] = myIPaddress[i] >> 8;
		RREQmessage[28+(i*2)+1] = myIPaddress[i] & 0xFF;
//		printf("Source IP address %i byte: %i\n", i*2, IPv6header[8+i*2]);
//		printf("Source IP address %i byte: %i\n", (i*2)+1, IPv6header[8+(i*2)+1]);
	}
	
	RREQmessage[44] = (mySequenceNumber >>24);
	RREQmessage[45] = (mySequenceNumber >>16); 
	RREQmessage[46] = (mySequenceNumber >>8); 
	RREQmessage[47] =  mySequenceNumber & 0xFF;
	
	for(int i=0; i<48; i++){
		//printf("RREQ message byte %i: %i\n",i, RREQmessage[i]);
	}
	printf("Generating RREQ message\n");
	
	return RREQmessage;
}

//Print to USART route entry data - function for debugging 
void routingTabelRouteDataprint(struct routingTableEntry routeNum){
	printf("routeEmpty: %i\n", routeNum.routeEmpty);

	for(int i=0; i<8; i++){
		printf("destinationIPaddress %i byte: %i\n", i, routeNum.destinationIPaddress[i]);
	}
	
	for(int i=0; i<8; i++){
		printf("NextHopIPaddress %i byte: %i\n", i, routeNum.nextHopIPaddress[i]);
	}
	
	printf("destinationSequenceNumber: %i\n", routeNum.destinationSequenceNumber);
	printf("validSequenceNumber: %i\n", routeNum.validSequenceNumber);
	printf("flags: %i\n", routeNum.flags);
	printf("hopCount: %i\n", routeNum.hopCount);
	printf("lifeTIme: %i\n", routeNum.lifeTime);
}

//generates this node IP address according to this node radio module MAC address
void initializeThisNodeIP(void){
	
	uint16_t thisNodeMAC[8];
	uint8_t missionID[2] = {0x00, 0x01};
	
	thisNodeMAC[5] = 0x00;
	thisNodeMAC[4] = 0x00;
	thisNodeMAC[3] = 0x00;
	thisNodeMAC[2] = 0x00;
	thisNodeMAC[1] = 0x00;
	thisNodeMAC[0] = MYNODEID;
	
	myIPaddress[0] = 0xFD <<8 | 0x00; // Set IPv6  prefix to FD , it means its unique local IPv6 address. Also set 1 Most significant byte IPv6 global ID to 0
	myIPaddress[1] = 0x00 <<8 | 0x00;
	myIPaddress[2] = 0x00 <<8 | 0x01; // this means that IPv6 global id (5 bytes) is 1
	myIPaddress[3] = 0x00 <<8 | 0x01; // Subnet ID is 1
	myIPaddress[4] = thisNodeMAC[5] << 8 | thisNodeMAC[4];
	myIPaddress[5] = thisNodeMAC[3] << 8 | missionID[0];
	myIPaddress[6] = missionID[1] << 8 | thisNodeMAC[2];
	myIPaddress[7] = thisNodeMAC[1] << 8 | thisNodeMAC[0];
	
	printf("Assigned this node  IP is: \n");
	for(int i = 0; i<8; i++){
		printf("IP address %i byte: %i \n", i, myIPaddress[i]);
	}
}

void updateARPtable(uint16_t *sourceIP, uint8_t sourceMAC){

	bool entryExists = false;
	printf("SourceMAC %i\n", sourceMAC);
	//check if entry for source MAC exists
	if(ARPtableEntry0.MACaddress == sourceMAC || ARPtableEntry1.MACaddress == sourceMAC || ARPtableEntry2.MACaddress == sourceMAC){
		entryExists = true;
		printf("ARP entry exists\n");
	}
	else{
		entryExists = false;
		printf("ARP entry DOES NOT exist\n");
	}
	
	//proceed next if there is no entry about source MAC
	if(!entryExists){
		printf("updateARPtable 2.0\n");
		//check which ARP table entries and if entry is empty write new data to it
		if(ARPtableEntry0.MACaddress == 0){
			//printf("updateARPtable 2.1\n");
			ARPtableEntry0.MACaddress = sourceMAC;
				for(int i=0; i<8; i++){
					ARPtableEntry0.IPaddress[i] = sourceIP[i];
				}
		}
		else if(ARPtableEntry1.MACaddress == 0){
			//printf("updateARPtable 2.2\n");
			ARPtableEntry1.MACaddress = sourceMAC;
				for(int i=0; i<8; i++){
					ARPtableEntry1.IPaddress[i] = sourceIP[i];
				}
		}
		else if(ARPtableEntry2.MACaddress == 0){
			//printf("updateARPtable 2.3\n");
			ARPtableEntry2.MACaddress = sourceMAC;
				for(int i=0; i<8; i++){
					ARPtableEntry2.IPaddress[i] = sourceIP[i];
				}
		}
		else{
			printf("There are no empty ARP table entries \n");
		}
	}
	
	for(int i =0; i<8; i++){
		//printf("ARP entry 0 IP address %i byte: %i\n", i, ARPtableEntry0.IPaddress[i]);
	}
	//printf("ARP entry 0 MAC address: %i\n", ARPtableEntry0.MACaddress);
	//printf("\n");
	for(int i =0; i<8; i++){
		//printf("ARP entry 1 IP address %i byte: %i\n", i, ARPtableEntry1.IPaddress[i]);
	}
	//printf("ARP entry 01MAC address: %i\n", ARPtableEntry1.MACaddress);
	//printf("\n");
		for(int i =0; i<8; i++){
		//printf("ARP entry 0 IP address %i byte: %i\n", i, ARPtableEntry2.IPaddress[i]);
	}
	//printf("ARP entry 0 MAC address: %i\n", ARPtableEntry2.MACaddress);
	sourceMAC =0;
}

uint32_t randTime(void){
	uint32_t randTimeVar = 0;
	uint16_t randTimeReturn =0;
	HAL_RNG_GenerateRandomNumber(&hrng, &randTimeVar);
	randTimeReturn = randTimeVar;
	printf("Random time to wait %i\n", randTimeReturn);
	return randTimeReturn;
}
//this function determines primary station PS which will receive data from other nodes and will send that data to ground station
void determinePrimaryStation(void){
	printf("Primary station IP address: \n");
	for(int i =0; i<8; i++){
		printf("%i\n", PrimaryStationIPaddress[i]); 
	}
	HAL_Delay(2000);
	uint8_t *PSstatementPointer = 0;
	uint8_t *PSstatementIPpacketPointer;
	thisNodeIsPS = true;
	if(thisNodeIsPS){
		PSstatementPointer = generatePSstatement(myIPaddress);
		HAL_Delay(200);
		BroadCastPSstatement(PSstatementPointer);
		HAL_Delay(400);
		BroadCastPSstatement(PSstatementPointer);
		HAL_Delay(600);
		BroadCastPSstatement(PSstatementPointer);
		HAL_Delay(500);
		BroadCastPSstatement(PSstatementPointer);
		
	}
}

void BroadCastPSstatement(uint8_t *PSstatementPtr){
		bool ifSendSucceded; 
		uint8_t *PSstatementIPpacketPointer;
		PSstatementIPpacketPointer = addIPv6header(myIPaddress, multicastIPaddress, 5, 17, PSstatementPtr);
		ifSendSucceded = sendWithStopAndWait(PSstatementIPpacketPointer, 57, 255);
}




void processPSmessage (uint8_t *PSmessage){
	static uint16_t *decodedPSIPaddr;
	uint16_t *decodedPSrepIPaddr;
	uint8_t ptrToPSMAC[6];
	uint8_t *PSstatPtr;
	uint8_t *broadcastPSmessagePtr;
	static uint8_t *regeneratedPSstatement;
	static uint8_t *PSstatementIPpacket;
	
	//check if PS statement or PS reply is received
	//PSmessage MSB is message type. If 0x01 --> PSstatement received 
	if(PSmessage[0] == 0x01){
		bool thisStationPSstatement = false;
		printf("Received PS statement message\n");
		//decode received PS statement
		decodedPSIPaddr=decodePSstatementMsg(PSmessage, ptrToPSMAC);
		thisStationPSstatement = compareTwoIPAddresses(myIPaddress, decodedPSIPaddr);
		
		if(!thisStationPSstatement){
			//check if this node MAC address is lower than received in PSmessage
			printf("ptrToOSMAC[5]: %i\n", ptrToPSMAC[5]);
			if((uint8_t)MYNODEID<ptrToPSMAC[5]){
				thisNodeIsPS = true;
				printf("This node's MAC is smaller than in the PS statement\n");
				PSstatPtr = generatePSreply(myIPaddress);
				PSstatementIPpacket = addIPv6header(myIPaddress, decodedPSIPaddr, 5, 18,  PSstatPtr);
				sendWithStopAndWait(PSstatementIPpacket, 58,ptrToPSMAC[5]);
				regeneratedPSstatement=generatePSstatement(PrimaryStationIPaddress);
				for(int i = 0; i<8; i++){
					PrimaryStationIPaddress[i]=0; 
				}
				for(int i=0; i<17; i++){
					printf("Re-broadcasted PS statement %i byte: %i\n", i,regeneratedPSstatement[i]);
				}
				//BroadCastPSstatement(regeneratedPSstatement);
				HAL_Delay(100);
				//BroadCastPSstatement(regeneratedPSstatement);
				HAL_Delay(300);
				//BroadCastPSstatement(regeneratedPSstatement);
			}
			else{
				printf("This node's MAC is higher than in the PS statement\n");
				thisNodeIsPS =false;
				for(int i = 0; i < 8; i++){
					PrimaryStationIPaddress[i] = decodedPSIPaddr[i];
					printf("PS station IP %i byte: %i \n", i, PrimaryStationIPaddress[i]);
				}
				regeneratedPSstatement=generatePSstatement(PrimaryStationIPaddress);
				for(int i=0; i<17; i++){
					printf("Re-broadcasted PS statement %i byte: %i\n", i,regeneratedPSstatement[i]);
				}
				//BroadCastPSstatement(regeneratedPSstatement);
				HAL_Delay(100);
				//BroadCastPSstatement(regeneratedPSstatement);
				HAL_Delay(300);
				//BroadCastPSstatement(regeneratedPSstatement);
			}
		}
	}
	//PSmessage MSB is message type. If 0x02 --> PSreply received 
	else if(PSmessage[0] == 0x02){
		printf("Received PS reply message\n");
		decodedPSrepIPaddr = decodePSreplytMsg(PSmessage, ptrToPSMAC);
		printf("Setting thisNodeIsPS flag to false. thisNodeIsPS: %i\n", thisNodeIsPS);
		thisNodeIsPS = false;
		printf("Setting PS IP address\n");
		for(int i = 0; i < 8; i++){
			PrimaryStationIPaddress[i] = decodedPSrepIPaddr[i];
			printf("PS station IP %i byte: %i\n", i, PrimaryStationIPaddress[i]);
		}
		regeneratedPSstatement=generatePSstatement(PrimaryStationIPaddress);
		for(int i=0; i<17; i++){
			printf("Re-broadcasted PS statement %i byte: %i\n", i,regeneratedPSstatement[i]);
		}
		BroadCastPSstatement(regeneratedPSstatement);
		HAL_Delay(200);
		BroadCastPSstatement(regeneratedPSstatement);
		HAL_Delay(300);
		BroadCastPSstatement(regeneratedPSstatement);
		HAL_Delay(300);
		BroadCastPSstatement(regeneratedPSstatement);
		HAL_Delay(100);
		BroadCastPSstatement(regeneratedPSstatement);
	}
}







//generates primary station (PS) statement message
//Message lenght: 17 Bytes
//message format: first 0th MSB byte is message type, following 1-16 MSB (bytes) are PS IP address. 
//statement message type: 1
uint8_t *generatePSstatement(uint16_t *PSIPaddr){
	static uint8_t PSstatemantMsg[17]; // here first byte is MSB
	PSstatemantMsg[0] = 0x01;
	for(int i=0; i<8; i++){
		PSstatemantMsg[1+i*2] = PSIPaddr[i] >> 8;
		PSstatemantMsg[1+(i*2)+1] = PSIPaddr[i] & 0xFF;
//		printf("PSstatemantMsg IP address %i byte: %i\n", i*2, PSstatemantMsg[1+i*2]);
//		printf("PSstatemantMsg IP address %i byte: %i\n", (i*2)+1, PSstatemantMsg[1+(i*2)+1]);
	}
	for(int i = 0; i<17 ;i++){
		printf("PSstatemantMsg  %i byte: %i\n", i, PSstatemantMsg[i]);
	}
	return PSstatemantMsg;
}

//generates primary station (PS) reply message
//message length: 18 bytes
//message format: 0th MSB bytes is message type, 1st MSB byte is reserved following 2-17 bytes are PS IP address. 
//reply message type is: 2
uint8_t *generatePSreply(uint16_t *PSIPaddr){
	static uint8_t PSreplyMsg[18]; // here first byte is MSB
	PSreplyMsg[0] = 0x02;
	PSreplyMsg[1] = 0x00;
	for(int i=0; i<8; i++){
		PSreplyMsg[2+i*2] = PSIPaddr[i] >> 8;
		PSreplyMsg[2+(i*2)+1] = PSIPaddr[i] & 0xFF;
//		printf("PSreplyMsg IP address %i byte: %i\n", i*2, PSreplyMsg[2+i*2]);
//		printf("PSreplyMsg IP address %i byte: %i\n", (i*2)+1, PSreplyMsg[2+(i*2)+1]);
	}
	for(int i = 0; i<18 ;i++){
		printf("PSreplyMsg  %i byte: %i\n", i, PSreplyMsg[i]);
	}
	return PSreplyMsg;
}

uint16_t *decodePSstatementMsg(uint8_t *PSstatementMessage, uint8_t *decodedPSMAC){
	uint8_t messageType = PSstatementMessage[0];
	static uint16_t decodedPSIPaddress[8];
	//decodedPSMAC[6];
	
	//convert PSstatementMesssage IP address from uint8_t type array to uint16_t type array.
	for(int i=0; i<8; i++){
		decodedPSIPaddress[i] = ( (uint16_t) PSstatementMessage[1+2*i] <<8 )| (uint16_t) PSstatementMessage[1+(i*2)+1];
		printf("Decoded PSstatement IP address %i\n", decodedPSIPaddress[i]);
	}
	decodedPSMAC[0] = decodedPSIPaddress[4] >>8;
	decodedPSMAC[1] = decodedPSIPaddress[4] & 0xFF;
	decodedPSMAC[2] = decodedPSIPaddress[5] >> 8;
	decodedPSMAC[3] = decodedPSIPaddress[6] & 0xFF;
	decodedPSMAC[4] = decodedPSIPaddress[7] >> 8;
	decodedPSMAC[5] = decodedPSIPaddress[7] & 0xFF;
	//returnedDecodedPSMAC = decodedPSMAC;
	for(int i=0; i<6; i++){
		printf("Decoded PS statement MAC addr %i byte; %i\n", i, decodedPSMAC[i]);
	}
	return decodedPSIPaddress;
}

uint16_t *decodePSreplytMsg(uint8_t *PSreplyMessage, uint8_t *decodedReplyPSMAC){
	uint8_t messageType = PSreplyMessage[0];
	static uint16_t decodedReplyPSIPaddress[8];
	//convert PSstatementMesssage IP address from uint8_t type array to uint16_t type array.
	for(int i=0; i<8; i++){
		decodedReplyPSIPaddress[i] = ( (uint16_t) PSreplyMessage[2+2*i] <<8 )| (uint16_t) PSreplyMessage[2+(i*2)+1];
		printf("Decoded PSreply IP address %i\n", decodedReplyPSIPaddress[i]);
	}
	
	decodedReplyPSMAC[0] = decodedReplyPSIPaddress[4] >>8;
	decodedReplyPSMAC[1] = decodedReplyPSIPaddress[4] & 0xFF;
	decodedReplyPSMAC[2] = decodedReplyPSIPaddress[5] >> 8;
	decodedReplyPSMAC[3] = decodedReplyPSIPaddress[6] & 0xFF;
	decodedReplyPSMAC[4] = decodedReplyPSIPaddress[7] >> 8;
	decodedReplyPSMAC[5] = decodedReplyPSIPaddress[7] & 0xFF;
		for(int i=0; i<6; i++){
		printf("Decoded PS reply MAC addr %i byte; %i\n", i, decodedReplyPSMAC[i]);
	}
	return decodedReplyPSIPaddress;
}

void node3Test(){
	uint32_t  start = millis();
	uint32_t end=millis();
	while (end-start<30000){
		HAL_Delay(50);
		IPpacketReceived(true);
		end = millis();
	}
	printPSinfo();
	IPpacketSend(PrimaryStationIPaddress, mazgas3Duom, 8, 0, 2);
	printPSinfo();
	HAL_Delay(40000);
	HAL_Delay(5000);
	IPpacketSend(PrimaryStationIPaddress, mazgas3Duom, 8, 0, 2);
	HAL_Delay(1000);
	determinePrimaryStation();
	HAL_Delay(800);
	start = millis();
	end=millis();
	while (end-start<5000){
		HAL_Delay(50);
		IPpacketReceived(true);
		end = millis();
	}
	IPpacketSend(PrimaryStationIPaddress, mazgas3Duom, 8, 0, 2);
}


